<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    public function show($usuario)
    {
       $u = User::findorFail($usuario);
        return view('usuarios.show', ['usuarios'=>$u]);
        return view('usuarios.show', compact('usuarios'));
    }
    public function edit($usuario){
        $u = User::findorFail($usuario);
        return view("usuarios.edit",['usuarios'=>$u]);
        return view('usuarios.edit', compact('usuarios'));
    }
    public function index(){ 
        $users = User::all();
        return view('usuarios.index',['usuarios'=>$users]);
        return view('usuarios.index', compact('usuarios'));

    }
    public function create(){
        return view('usuarios.create');
    }
    public function store(Request $request) {
        $usuario = new User();
        $usuario->name= $request->name;
        $usuario->email = $request->email;
        $usuario->password = bcrypt($request->password);
        $usuario->created_at = now();
        $usuario->save();
        return redirect()->action([UsuarioController::class, 'index']);

    }
    public function update(Request $request, $id) {
        $usuario = User::find($id);
        $usuario->name= $request->name;
        $usuario->email = $request->email;
        $usuario->created_at = $request->created_at;
        $usuario->save();
        return redirect()->action([UsuarioController::class, 'index']);

    }
}
