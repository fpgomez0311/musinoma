<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->delete();
        $this->call(UserSeeder::class);
        \App\Models\User::factory(10)->create();
        
        DB::table('eventos')->delete();
        $this->call(EventosSeeder::class);
        \App\Models\Evento::factory(5)->create();
    }
}
