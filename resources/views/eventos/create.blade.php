@extends('layouts.master')

@section('titulo')
    Crear evento
@endsection

@section("contenido")
<div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">Crear evento</div>
                <div class="card-body"
                     style="padding:30px">
                    <form action="{{route("eventos.store")}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="especie">Nombre:</label>
                            <input type="text" name="nombre" id="nombre" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="descripcion">Descripción:</label>
                            <textarea name="descripcion"
                                      id="descripcion"
                                      class="form-control"
                                      rows="3"></textarea>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
                                Crear evento
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection