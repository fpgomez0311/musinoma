<?php

namespace Database\Factories;

use App\Models\Evento;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


class EventoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Evento::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->company,
            'fecha_creacion' => $this->faker->date,
            'descripcion' =>$this->faker->text,
            'miembros' => $this->faker->randomDigit,
            'updated_at' => $this->faker->date,
            'created_at' => $this->faker->date
        ];
    }
}
