<?php

use App\Http\Controllers\EventoController;
use App\Http\Controllers\UsuarioController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.home');
});


Route::get('eventos', [EventoController::class, 'index'])->name('eventos.index');
Route::get('eventos/{evento}',[EventoController::class, 'show'])->name('eventos.show');
Route::get('eventos/crear', [EventoController::class, 'create'])->name('eventos.create')->middleware('auth');
Route::post('eventos/crear',[EventoController::class, 'store'])->name('eventos.store');

Route::get('usuarios', [UsuarioController::class, 'index'])->name('usuarios.index');
Route::get('usuarios/{usuario}', [UsuarioController::class, 'show'])->name('usuarios.show');
Route::get('usuarios/{usuario}/editar',[UsuarioController::class, 'edit'])->name('usuarios.edit')->middleware('auth');
Route::get('registro', [UsuarioController::class, 'create'])->name('usuarios.create')->middleware('auth');
Route::post('registro',[UsuarioController::class, 'store'])->name('usuarios.store');
Route::put('usuarios/{usuario}/editar',[UsuarioController::class, 'update'])->name('usuarios.update');
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
