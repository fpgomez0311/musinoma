<nav class="navbar navbar-expand-sm navbar-dark" style="background-color: #000000;">
  <div class="container-fluid">
  <a class="navbar-brand" href="{{url('/')}}">musinoma</a>
  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
      {{--@if(Auth::check() )--}}
      
      <li class="nav-item">
        <a href="{{route("usuarios.index")}}" class="nav-link" >Usuarios</a>
      </li>
      <li class="nav-item">
        <a href="{{route("eventos.index")}}" class="nav-link">Eventos</a>
      </li>
      <li class="nav-item">
        <a href="{{route("usuarios.create")}}" class="nav-link">Añadir Usuario</a>
      </li>
      <li class="nav-item">
        <a href="{{route("eventos.create")}}"  class="nav-link">Crear evento</a>
      </li>
      <li class="nav-item">
        <a href="{{url('register')}}" class="nav-link">Registrarse</a>
      </li>
    </ul>
    {{--@endif --}}
    @if(Auth::check() )
        <form class="d-flex">
          <input id="busqueda" class="form-control mr-sm-3" type="text" placeholder="Buscar" aria-label="Buscar">
        </form>
        
        <ul class="navbar-nav navbar-right">
          <li class="nav-item">
            <a href="{{ route('profile.show') }}"  class="nav-link">
              {{ Auth::user()->name }}
            </a>
          </li>
            <li class="nav-item">
                <a href="{{ route('logout') }}"  class="nav-link"
                  onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();" >
                    <span class="glyphicon glyphicon-off"></span>
                    Cerrar sesión
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    @else
        <ul class="navbar-nav navbar-right">
            <li class="nav-item">
              <a href="{{url('login')}}" class="nav-link">Login</a>
            </li>
        </ul>
    @endif 
  </div>
</div>
</nav>
