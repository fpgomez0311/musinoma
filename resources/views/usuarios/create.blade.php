
@extends('layouts.master')

@section('titulo')
    Registrar usuario
@endsection

@section("contenido")
    <form class="col" action="{{route("usuarios.store")}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col form-group">
                <label for="nombre">Nombre:</label>
                <input type="text" class="form-control" name="name" id="name" aria-describedby="helpId"
                       placeholder="Introduzca un nombre">
            </div><br>
            <div class="col form-group">
                <label for="nombre">Email:</label>
                <input type="text" class="form-control" name="email" id="email" aria-describedby="helpId"
                       placeholder="Introduzca un email">
            </div><br>
            <div class="col form-group">
                <label for="nombre">Contraseña:</label>
                <input type="text" class="form-control" name="password" id="password" aria-describedby="helpId"
                       placeholder="Introduzca una contraseña">
            </div>
            
        <button type="submit" name="submit" class="row btn btn-primary">Registrarse</button>
    </form>
@endsection