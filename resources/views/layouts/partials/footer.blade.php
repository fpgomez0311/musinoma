        <footer>
        	<div class="footer-top">
		        <div class="container">
		        	<div class="row">
		        		<div class="col-md-3 footer-about wow fadeInUp">
		        			<h3>Sobre nosotros:</h3>
		        			<p>
		        				Somos una red social para amantes de la música.
		        			</p>
		        			<p>&copy; musinoma </p>
	                    </div>
		        		<div class="col-md-4 offset-md-1 footer-contact wow fadeInDown">
		        			<h3>Contacto</h3>
		                	<p><i class="fas fa-map-marker-alt"></i> Calle Prueba 10, 10136 Torrelavega Cantabria</p>
		                	<p><i class="fas fa-phone"></i> Telefono: 6569350177</p>
		                	<p><i class="fas fa-envelope"></i> Email: <a href="mailto:fernando.palaciogomez@iesmiguelherrero.com">fernando.palaciogomez@iesmiguelherrero.com</a></p>
		                	<p><i class="fab fa-skype"></i> Skype: Fernando</p>
	                    </div>
	                    <div class="col-md-4 footer-links wow fadeInUp">
	                    	<div class="row">
	                    		<div class="col">
	                    			<h3>Links</h3>
	                    		</div>
	                    	</div>
	                    	<div class="row">
	                    		<div class="col-md-6">
	                    			<p><a class="scroll-link" href="#top-content">Home</a></p>
	                    			<p><a href="#">Herramientas</a></p>
	                    			<p><a href="#">Funcionamiento</a></p>
	                    			<p><a href="#">Apartado clientes</a></p>
	                    		</div>
	                    		<div class="col-md-6">
	                    			<p><a href="#">Condiciones &amp; uso</a></p>
	                    			<p><a href="#">Afiliados</a></p>
	                    			<p><a href="#">Términos</a></p>
	                    		</div>
	                    	</div>
	                    </div>
		            </div>
		        </div>
	        </div>
	        <div class="footer-bottom">
	        	<div class="container">
	        		<div class="row">
	           			<div class="col footer-social">
	                    	<a href="#"><i class="fab fa-facebook-f"></i></a> 
							<a href="#"><i class="fab fa-twitter"></i></a> 
							<a href="#"><i class="fab fa-google-plus-g"></i></a> 
							<a href="#"><i class="fab fa-instagram"></i></a> 
							<a href="#"><i class="fab fa-pinterest"></i></a>
	                    </div>
	           		</div>
	        	</div>
	        </div>
        </footer>

