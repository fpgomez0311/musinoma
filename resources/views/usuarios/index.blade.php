@extends('layouts.master')
@section('titulo')
Usuarios
@endsection
@section('contenido')

<br><br>
<div class="row">
@foreach( $usuarios as $clave => $usuario )
<div class="card" style="width:400px">
  <img class="card-img-top" src="{{asset('assets/imagenes/usuario.png')}}" alt="Card image">
  <div class="card-body">
    <h4 class="card-title">{{$usuario->name}}</h4>
    <p class="card-text">{{$usuario->email}}</p>
    <a href="{{ route('usuarios.show' , $usuario->id ) }}" class="btn btn-primary">Ver perfil</a>
  </div>
</div>
@endforeach
</div>

@endsection
