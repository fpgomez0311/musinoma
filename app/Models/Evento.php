<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Evento extends Model
{
    use HasFactory;

    protected $table = "eventos";


    public function getDias()
    {
        $fechaFormateada = Carbon::parse($this->fecha_creacion);
        return $fechaFormateada->diffInDays(Carbon::now());
    }

    protected $fillable = [
        'nombre',
        'fecha_creacion',
        'descripcion',
        'miembros'
    ];
    public function users() {
        return $this->belongsToMany('App\User');
    }
}
