@extends('layouts.master')
@section('titulo')
Ver evento
@endsection
@section('contenido')
<div class="row">
<div class="col-sm-3"><br>
<figure class="figure">
  <img src="{{asset('assets/imagenes/evento.png')}}" class="figure-img img-fluid rounded">
</figure>
</div>
<div class="col-sm-9">
<p class="h1">{{$eventos->nombre}}</p>
<p class="h4">Fecha creación:</p>
<p class="lead">{{$eventos->fecha_creacion}}   (hace  {{$eventos->getDias()}} días)</p>
<p class="h4">Miembros::</p>
<p class="lead">{{$eventos->miembros}}</p>
<p class="h4">Descripción:</p>
<p class="lead">{{$eventos->descripcion}}</p>
</a>
<a href="{{route("eventos.index")}}"><button type="button" class="btn btn-dark">Volver a eventos</button></a>
<br>
</div>
</div>

@endsection