@extends("layouts.master")

@section("titulo")
    Editar usuario
@endsection

@section("contenido")
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">Editar usuario {{$usuarios->name}}</div>
                <div class="card-body"
                     style="padding:30px">
                    <form action="{{route("usuarios.update", $usuarios)}}" method="post" enctype="multipart/form-data">
                    @method("put")
                        @csrf
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" name="name" id="name" class="form-control" required
                                   value="{{$usuarios->name}}">
                        </div>
                        <div class="form-group">
                            <label for="peso">Email: </label>
                            <input type="email" class="form-control" name="email" value="{{$usuarios->email}}"
                                   required>
                        </div>
                        
                        <div class="form-group">
                            <label for="fechaNacimiento">Fecha de registro: </label>
                            <input type="date" class="form-control" name="created_at"
                                   value="{{$usuarios->created_at}}"
                                   required>
                        </div>
                        
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
                                Editar
                                usuario
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
