@extends('layouts.master')
@section('titulo')
Eventos
@endsection
@section('contenido')
<div class="row">
@foreach( $eventos as $clave => $evento )
<div class="card-group">
  <div class="card">
    <img class="card-img-top" src="{{asset('assets/imagenes/evento.png')}}" width="100px" height="100px" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title"><b><a href="{{ route('eventos.show' , $evento->id ) }}">{{$evento->nombre}}</a></b></h5>
      <p class="card-text">{{$evento->descripcion}}</p>
      <p class="card-text"><small class="text-muted">Fecha de Creación: {{$evento->fecha_creacion}}</small></p>
    </div>
@endforeach
</div>


@endsection
