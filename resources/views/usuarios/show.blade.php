@extends('layouts.master')
@section('titulo')
Ver perfil
@endsection
@section('contenido')
<div class="row">
<div class="col-sm-3"><br>
<figure class="figure">
  <img src="{{asset('assets/imagenes/usuario.png')}}" class="figure-img img-fluid rounded">
</figure>
</div>
<div class="col-sm-9">
<p class="h1">{{$usuarios->name}}</p>
<p class="h4">Email:</p>
<p class="lead">{{$usuarios->email}}</p>
<p class="h4">Fecha de Registro:</p>
<p class="lead">{{$usuarios->created_at}}</p>
<p class="h4">Descripción:</p>
<p class="lead">Este es un usuario de nuestra web.</p>
<a href="{{route("usuarios.edit", $usuarios->id)}}"><button type="button" class="btn btn-secondary">Editar usuario</button>
</a>
<a href="{{route("usuarios.index")}}"><button type="button" class="btn btn-dark">Volver a usuarios</button></a>
<br>
</div>
</div>

@endsection