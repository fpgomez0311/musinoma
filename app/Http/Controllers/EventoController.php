<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Evento;
class EventoController extends Controller
{
    public function index(){ 
        $eventos = Evento::all();
        return view('eventos.index',['eventos'=>$eventos]);
        return view('eventos.index', compact('eventos'));

    }
    public function show($evento)
    {
       $e = Evento::findorFail($evento);
        return view('eventos.show', ['eventos'=>$e]);
        return view('eventos.show', compact('eventos'));
    }
    public function create(){
        return view('eventos.create');
    }
    public function store(Request $request) {
        $evento = new Evento();
        $evento->nombre= $request->nombre;
        $evento->fecha_creacion = now();
        $evento->descripción = $request->descripcion;
        $evento->miembros = 1;
        $evento->save();
        return redirect()->action([EventoController::class, 'index']);

    }
}
